﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;

namespace ImageToZ80
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Monochrome Image to z80 Assembly Converter");
            string input = null, output = null;
            string assembly = "";
            string hexPrefix = "$", hexPostfix = "", binPrefix = "%", binPostfix = "";
            int numberBase = 16;
            int padding = 2, width = -1;
            string prefix, postfix;
            for (int i = 0; i < args.Length; i++)
            {
                var arg = args[i];
                if (arg.StartsWith("-"))
                {
                    switch (arg)
                    {
                        case "--help":
                            OutputUsage();
                            return;
                        case "--hex-prefix":
                            hexPrefix = args[++i];
                            break;
                        case "--hex-postfix":
                            hexPostfix = args[++i];
                            break;
                        case "--bin-prefix":
                            binPrefix = args[++i];
                            break;
                        case "--bin-postfix":
                            binPostfix = args[++i];
                            break;
                        case "--type":
                            numberBase = args[++i] == "hex" ? 16 : 2;
                            padding = numberBase == 2 ? 8 : 2;
                            break;
                        case "--pad":
                            padding = int.Parse(args[++i]);
                            break;
                        case "--width":
                            width = int.Parse(args[++i]);
                            break;
                        default:
                            Console.WriteLine("Error: Unrecognized parameter: " + arg);
                            OutputUsage();
                            return;
                            break;
                    }
                }
                else
                {
                    if (input == null)
                        input = arg;
                    else if (output == null)
                        output = arg;
                    else
                    {
                        Console.WriteLine("Error: Unrecognized parameter: " + arg);
                        OutputUsage();
                        return;
                    }
                }
            }

            if (numberBase == 16)
            {
                prefix = hexPrefix;
                postfix = hexPostfix;
            }
            else
            {
                prefix = binPrefix;
                postfix = binPostfix;
            }

            if (input == null)
            {
                Console.WriteLine("No input files specified. Use --help to see usage.");
                return;
            }

            var bitmap = (Bitmap)Image.FromFile(input);
            byte mask = 0x80;
            byte value = 0;
            int currentWidth = 0;
            string db;
            if (width == -1)
            {
                width = bitmap.Width / 8;
                if (bitmap.Width % 8 != 0) width++;
            }
            assembly = ".db ";
            for (int y = 0; y < bitmap.Height; y++)
            {
                for (int x = 0; x < bitmap.Width; x++)
                {
                    var pixel = bitmap.GetPixel(x, y);
                    if (pixel.R < 128)
                        value |= mask;
                    mask >>= 1;
                    if (mask == 0)
                    {
                        currentWidth++;
                        db = Convert.ToString(value, numberBase);
                        while (db.Length < padding)
                            db = "0" + db;
                        db = prefix + db + postfix;
                        assembly += db;
                        if (currentWidth == width)
                        {
                            currentWidth = 0;
                            assembly += Environment.NewLine + ".db ";
                        }
                        else
                            assembly += ", ";
                        mask = 0x80;
                        value = 0;
                    }
                }
                if (mask != 0x80)
                {
                    currentWidth++;
                    db = Convert.ToString(value, numberBase);
                    while (db.Length < padding)
                        db = "0" + db;
                    db = prefix + db + postfix;
                    assembly += db;
                    if (currentWidth == width)
                    {
                        currentWidth = 0;
                        assembly += Environment.NewLine + ".db ";
                    }
                    else
                        assembly += ", ";

                    mask = 0x80;
                    value = 0;
                }
            }
            if (assembly.EndsWith(", "))
                assembly = assembly.Remove(assembly.Length - 2);
            else if (assembly.EndsWith(".db "))
                assembly = assembly.Remove(assembly.Length - 5);
            if (output == null)
                Console.Write(assembly);
            else
                File.WriteAllText(output, assembly);
        }

        static void OutputUsage()
        {
            Console.WriteLine("Usage:\n" +
                "ImageToZ80 (options) [image file] (output file)\n" +
                "If no output file is specified, stdout is used.\n" +
                "Supported image formats: BMP, GIF, EXIF, JPG, PNG, TIFF\n" +
                "Options:\n" +
                "--hex-prefix [prefix]: Specifies a format other than $xx for hex numbers.\n" +
                "--hex-postfix [postfix]: Specifies a format other than $xx for hex numbers.\n" +
                "--bin-prefix [prefix]: Specifies a format other than %xx for binary numbers.\n" +
                "--bin-postfix [postfix]: Specifies a format other than %xx for binary numbers.\n" +
                "--type [hex|binary]: Specifies the output format. Default is hex.\n" +
                "--pad [amount]: Pads outputted values. For instance, $9 becomes $09 with\n\t" +
                    "--pad 2. Default is 2 for hex, and 8 for binary.\n" +
                "--width [amount]: Number of bytes per row in the output. Default is the\n\t" +
                    "width of the image divided by 8.\n" +
                "--help: Output usage.");
        }
    }
}
